﻿using System;
using System.Diagnostics;
using System.Windows;

namespace ExampleModelViewer {
    /// <summary>
    /// Desktop application that prepares a 3D model into a format that can be rendered into an HTML page.
    /// </summary>
    public partial class MainWindow {
        private const string Address = "http://localhost:3000";
        
        public MainWindow() { InitializeComponent(); }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e) {
            try {
                ChromiumWebBrowser.Address = Address;
            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }

        private void MenuItem_OnClick(object sender, RoutedEventArgs e) {
            // Opens default browser.
            Process.Start(Address);            
        }
    }
}
